
#ifndef AKU_MYM_H
#define AKU_MYM_H

#import <Foundation/Foundation.h>
#include <aku/AKU.h>

AKU_API void	AKUMymInit	();
void AKUMYMAppDidFinishLaunching();
void AKUMYMAppDidBecomeActive();
void AKUMYMAppWillResignActive();
void AKUMYMAppWillEnterForeground();
void AKUMYMAppDidEnterBackground();
void AKUMYMAPpWillTerminate();
void AKUMYMAppOpenFromURL (NSURL *url, NSString *sourceApp);

bool AKUMYMisAutoRender();

#endif
