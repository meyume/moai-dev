
#include <aku/AKU-mym.h>
#include <mym-ios/MYMMOAIAppIOS.h>
#include <mym-ios/MYMMOAIShareIOS.h>
//#include <mym-ios/playhaven/MYMMOAIPlayhavenIOS.h>
#include <mym-ios/chartboost/MYMMOAIChartBoostIOS.h>
#include <mym-ios/facebook/MYMMOAIFacebookIOS.h>
//#include <mym-ios/flurry/MYMMOAIFlurryIOS.h>
#include <mym-ios/tournament/MYMMOAITournamentIOS.h>
//#include <mym-ios/audio/MYMMOAIAudioIOS.h>
#include <mym-ios/amazon/MYMMOAIAmazonGameCircleIOS.h>
#include <mym-ios/amazon/MYMMOAIAmazonAnalyticsIOS.h>

//----------------------------------------------------------------//
void AKUMymInit () {
	
	MYMMOAIAppIOS::Affirm();
	REGISTER_LUA_CLASS (MYMMOAIAppIOS);
	
	//MYMMOAIPlayhavenIOS::Affirm();
	//REGISTER_LUA_CLASS (MYMMOAIPlayhavenIOS);
	
	MYMMOAIChartBoostIOS::Affirm();
	REGISTER_LUA_CLASS(MYMMOAIChartBoostIOS);
	
	MYMMOAIFacebookIOS::Affirm();
	REGISTER_LUA_CLASS(MYMMOAIFacebookIOS);
	
	//MYMMOAIFlurryIOS::Affirm();
	//REGISTER_LUA_CLASS(MYMMOAIFlurryIOS);
	
	MYMMOAITournamentIOS::Affirm();
	REGISTER_LUA_CLASS(MYMMOAITournamentIOS);
	
	//MYMMOAIAudioIOS::Affirm();
	//REGISTER_LUA_CLASS(MYMMOAIAudioIOS);
	
	MYMMOAIAmazonGameCircleIOS::Affirm();
	REGISTER_LUA_CLASS(MYMMOAIAmazonGameCircleIOS);
	
	MYMMOAIAmazonAnalyticsIOS::Affirm();
	REGISTER_LUA_CLASS(MYMMOAIAmazonAnalyticsIOS);
	
	MYMMOAIShareIOS::Affirm();
	REGISTER_LUA_CLASS(MYMMOAIShareIOS);
	
}

void AKUMYMAppDidFinishLaunching() {
#ifdef USE_AMZN_GAMECIRCLE
	MYMMOAIAmazonGameCircleIOS::Get().onApplicationDidFinishLaunching();
#endif
}

void AKUMYMAppDidBecomeActive() {
	MYMMOAIAppIOS::Get().appDidBecomeActive();
}

void AKUMYMAppWillResignActive() {
	MYMMOAIAppIOS::Get().appWillResignActive();
}

void AKUMYMAppWillEnterForeground() {
	MYMMOAIAppIOS::Get().appWillEnterForeground();
}

void AKUMYMAppDidEnterBackground() {
	MYMMOAIAppIOS::Get().appDidEnterBackground();
}

void AKUMYMAPpWillTerminate() {
	MYMMOAIAppIOS::Get().appWillTerminate();
}

void AKUMYMAppOpenFromURL (NSURL *url, NSString *sourceApp) {
	MYMMOAIFacebookIOS::Get().handleOpenUrl(url, sourceApp);
	MYMMOAIAppIOS::Get().appOpenFromUrl(url, sourceApp);
#ifdef USE_AMZN_GAMECIRCLE
	MYMMOAIAmazonGameCircleIOS::Get().handleOpenUrl(url, sourceApp);
#endif
}

bool AKUMYMisAutoRender() {
	return MYMMOAIAppIOS::Get().autoRender;
}
