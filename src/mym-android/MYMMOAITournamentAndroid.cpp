/*
 * Copyright (c) Xtremics Ltd All rights reserved.
 *
 * This software is the confidential and proprietary information of Xtremics
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with Xtremics.
 */
#include "pch.h"

#include <jni.h>

#include <moaiext-android/moaiext-jni.h>
#include <mym-android/MYMMOAITournamentAndroid.h>

extern JavaVM* jvm;

int MYMMOAITournamentAndroid::_init( lua_State* L ) {
	MOAILuaState state(L);
	
	JNI_GET_ENV (jvm, env);
	
	cc8* serverUrl = lua_tostring(state, 1);
	cc8* secretKey = lua_tostring(state, 2);
	JNI_GET_JSTRING (serverUrl, jserverUrl);
	JNI_GET_JSTRING (secretKey, jsecretKey);
	
	jclass tournament = env->FindClass("com/meyume/moai/MYMTournamentClient");
	if (tournament == NULL) {
		USLog::Print ("MYMMOAITournamentAndroid: Unable to find java class %s", "com/meyume/moai/MYMTournamentClient");
	} else {
		jmethodID init = env->GetStaticMethodID (tournament, "init", "(Ljava/lang/String;Ljava/lang/String;)V");
		if (init == NULL) {
			USLog::Print ("MYMMOAITournamentAndroid: Unable to find static java method %s", "init");
		} else {
			env->CallStaticVoidMethod(tournament, init, jserverUrl, jsecretKey);
		}
	}
	
	return 0;
}

int MYMMOAITournamentAndroid::_login( lua_State* L ) {
	MOAILuaState state(L);
	
	JNI_GET_ENV (jvm, env);
	
	cc8* data = lua_tostring(state, 1);
	JNI_GET_JSTRING (data, jdata);
	
	jclass tournament = env->FindClass("com/meyume/moai/MYMTournamentClient");
	if (tournament == NULL) {
		USLog::Print ("MYMMOAITournamentAndroid: Unable to find java class %s", "com/meyume/moai/MYMTournamentClient");
	} else {
		jmethodID login = env->GetStaticMethodID (tournament, "login", "(Ljava/lang/String;)Z");
		if (login == NULL) {
			USLog::Print ("MYMMOAITournamentAndroid: Unable to find static java method %s", "login");
		} else {
			jboolean ret = (jboolean)env->CallStaticBooleanMethod(tournament, login, jdata);
			lua_pushboolean(state, ret);
			return 1;
		}
	}
	
	lua_pushboolean(state, false);
	return 1;
}

int MYMMOAITournamentAndroid::_postScore( lua_State* L ) {
	MOAILuaState state(L);
	
	JNI_GET_ENV (jvm, env);
	
	cc8* data = lua_tostring(state, 1);
	JNI_GET_JSTRING (data, jdata);
	
	jclass tournament = env->FindClass("com/meyume/moai/MYMTournamentClient");
	if (tournament == NULL) {
		USLog::Print ("MYMMOAITournamentAndroid: Unable to find java class %s", "com/meyume/moai/MYMTournamentClient");
	} else {
		jmethodID postScore = env->GetStaticMethodID (tournament, "postScore", "(Ljava/lang/String;)Z");
		if (postScore == NULL) {
			USLog::Print ("MYMMOAITournamentAndroid: Unable to find static java method %s", "postScore");
		} else {
			jboolean ret = (jboolean)env->CallStaticBooleanMethod(tournament, postScore, jdata);
			lua_pushboolean(state, ret);
			return 1;
		}
	}
	
	lua_pushboolean(state, false);
	return 1;
}

int MYMMOAITournamentAndroid::_setListener( lua_State* L ) {
	MOAILuaState state(L);
	
	u32 idx = state.GetValue<u32>(1, TOTAL);
	
	if (idx < TOTAL) {
		MYMMOAITournamentAndroid::Get().mListeners[idx].SetStrongRef(state, 2);
	}
	
	return 0;
}

MYMMOAITournamentAndroid::MYMMOAITournamentAndroid() {
	RTTI_SINGLE(MOAILuaObject)
}

MYMMOAITournamentAndroid::~MYMMOAITournamentAndroid() {
}

void MYMMOAITournamentAndroid::RegisterLuaClass(MOAILuaState& state) {
	state.SetField ( -1, "DID_RECEIVE_DATA",		( u32 )DID_RECEIVE_DATA );
	
	luaL_Reg regTable[] = {
		{ "init", _init},
		{ "login",					_login },
		{ "postScore",				_postScore },
		{ "setListener",			_setListener },
		{NULL, NULL}
	};

	luaL_register(state, 0, regTable);
}


void MYMMOAITournamentAndroid::notifyDidReceiveData(cc8* data, cc8* action) {
	MOAILuaRef& callback = this->mListeners[DID_RECEIVE_DATA];
		
	if (callback) {
		MOAILuaStateHandle state = callback.GetSelf();
		lua_pushstring(state, action);
		lua_pushstring(state, data);
		state.DebugCall ( 2, 0 );
	}
}

extern "C" void Java_com_meyume_moai_MYMTournamentClient_AKUMYMNotifyTournamentClientDidReceiveData ( JNIEnv* env, jclass obj, jstring data, jstring action ) {
	if (data == NULL) {
		return;
	}
	if (action == NULL) {
		return;
	}
	MYMMOAITournamentAndroid::Get().notifyDidReceiveData((cc8*)env->GetStringUTFChars(data, 0), (cc8*)env->GetStringUTFChars(action, 0));
}
