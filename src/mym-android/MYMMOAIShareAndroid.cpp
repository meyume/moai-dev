/*
 * Copyright (c) Xtremics Ltd All rights reserved.
 *
 * This software is the confidential and proprietary information of Xtremics
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with Xtremics.
 */
#include "pch.h"

#include <jni.h>

#include <moaiext-android/moaiext-jni.h>
#include <mym-android/MYMMOAIShareAndroid.h>

extern JavaVM* jvm;

int MYMMOAIShareAndroid::_init(lua_State* L) {
	MOAILuaState state(L);
	
	JNI_GET_ENV (jvm, env);
	
	jclass mymShare = env->FindClass("com/meyume/moai/MYMShare");
	if (mymShare == NULL) {
		USLog::Print ("MYMMOAIShareAndroid: Unable to find java class %s", "com/meyume/moai/MYMShare");
	} else {
		jmethodID init = env->GetStaticMethodID (mymShare, "init", "()V");
		if (init == NULL) {
			USLog::Print ("MYMMOAIShareAndroid: Unable to find static java method %s", "init");
		} else {
			env->CallStaticVoidMethod(mymShare, init);
		}
	}
	
	return 0;
}

int MYMMOAIShareAndroid::_share(lua_State* L) {
	MOAILuaState state(L);
	
	cc8* title = lua_tostring(state, 1);
	cc8* msg = lua_tostring(state, 2);
	cc8* url = lua_tostring(state, 3);
	cc8* imgUrl = lua_tostring(state, 4);
	
	JNI_GET_ENV (jvm, env);
	
	JNI_GET_JSTRING (title, jtitle);
	JNI_GET_JSTRING (msg, jmsg);
	JNI_GET_JSTRING (url, jurl);
	JNI_GET_JSTRING (imgUrl, jimgUrl);
	
	jclass mymShare = env->FindClass("com/meyume/moai/MYMShare");
	if (mymShare == NULL) {
		USLog::Print ("MYMMOAIShareAndroid: Unable to find java class %s", "com/meyume/moai/MYMShare");
	} else {
		jmethodID share = env->GetStaticMethodID (mymShare, "share", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V");
		if (share == NULL) {
			USLog::Print ("MYMMOAIShareAndroid: Unable to find static java method %s", "share");
		} else {
			env->CallStaticVoidMethod(mymShare, share, jtitle, jmsg, jurl, jimgUrl);
		}
	}
	
	return 0;
}

MYMMOAIShareAndroid::MYMMOAIShareAndroid() {
	RTTI_SINGLE(MOAILuaObject)
}

MYMMOAIShareAndroid::~MYMMOAIShareAndroid() {
}

void MYMMOAIShareAndroid::RegisterLuaClass(MOAILuaState& state) {
	luaL_Reg regTable[] = {
		{"init", _init},
		{"share", _share},
		{NULL, NULL}
	};

	luaL_register(state, 0, regTable);
}
