/*
 * Copyright (c) Xtremics Ltd All rights reserved.
 *
 * This software is the confidential and proprietary information of Xtremics
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with Xtremics.
 */
 
#include <moaicore/moaicore.h>

class MYMMOAIFBTournament : 
	public MOAIGlobalClass < MYMMOAIFBTournament, MOAILuaObject > {

private:
	static int _init(lua_State* L);
	static int _login( lua_State* L );
	static int _isProcessing( lua_State* L );
	static int _setListener( lua_State* L );

public:
	DECL_LUA_SINGLETON(MYMMOAIFBTournament);
	
	enum {
		RETRIEVED_SCORES,
		TOTAL,
	};
	
	MOAILuaRef mListeners[TOTAL];
	
	MYMMOAIFBTournament();
	~MYMMOAIFBTournament();
	void RegisterLuaClass(MOAILuaState& state);
	
	void notifyFBDidRetrieveScores(cc8* jsonScores, cc8* jsonUser, cc8* jsonPermissions);

};
