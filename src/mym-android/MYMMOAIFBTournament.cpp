/*
 * Copyright (c) Xtremics Ltd All rights reserved.
 *
 * This software is the confidential and proprietary information of Xtremics
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with Xtremics.
 */
#include "pch.h"

#include <jni.h>

#include <moaiext-android/moaiext-jni.h>
#include <mym-android/MYMMOAIFBTournament.h>

extern JavaVM* jvm;

int MYMMOAIFBTournament::_init(lua_State* L) {
	MOAILuaState state(L);
	
	JNI_GET_ENV (jvm, env);
	
	jclass facebook = env->FindClass("com/meyume/moai/fbtournament/MYMFBTournament");
	if (facebook == NULL) {
		USLog::Print ("MYMMOAIFBTournament: Unable to find java class %s", "com/meyume/moai/fbtournament/MYMFBTournament");
	} else {
		jmethodID init = env->GetStaticMethodID (facebook, "init", "()V");
		if (init == NULL) {
			USLog::Print ("MYMMOAIFBTournament: Unable to find static java method %s", "init");
		} else {
			env->CallStaticVoidMethod(facebook, init);
		}
	}
	
	return 0;
}

int MYMMOAIFBTournament::_login( lua_State* L ) {
	MOAILuaState state(L);
	
	JNI_GET_ENV (jvm, env);
	
	jclass facebook = env->FindClass("com/meyume/moai/fbtournament/MYMFBTournament");
	if (facebook == NULL) {
		USLog::Print ("MYMMOAIFBTournament: Unable to find java class %s", "com/meyume/moai/fbtournament/MYMFBTournament");
	} else {
		jmethodID login = env->GetStaticMethodID (facebook, "login", "()V");
		if (login == NULL) {
			USLog::Print ("MYMMOAIFBTournament: Unable to find static java method %s", "login");
		} else {
			env->CallStaticVoidMethod(facebook, login);
		}
	}
	
	return 0;
}

int MYMMOAIFBTournament::_isProcessing( lua_State* L ) {
	MOAILuaState state(L);
	
	JNI_GET_ENV (jvm, env);
	
	jboolean ret = false;
	
	jclass facebook = env->FindClass("com/meyume/moai/fbtournament/MYMFBTournament");
	if (facebook == NULL) {
		USLog::Print ("MYMMOAIFBTournament: Unable to find java class %s", "com/meyume/moai/fbtournament/MYMFBTournament");
	} else {
		jmethodID isProcessing = env->GetStaticMethodID (facebook, "isProcessing", "()Z");
		if (isProcessing == NULL) {
			USLog::Print ("MYMMOAIFBTournament: Unable to find static java method %s", "isProcessing");
		} else {
			ret = (jboolean)env->CallStaticBooleanMethod(facebook, isProcessing);
		}
	}
	
	lua_pushboolean(state, ret);
	return 1;
}

int MYMMOAIFBTournament::_setListener( lua_State* L ) {
	MOAILuaState state(L);
	
	u32 idx = state.GetValue<u32>(1, TOTAL);
	
	if (idx < TOTAL) {
		MYMMOAIFBTournament::Get().mListeners[idx].SetStrongRef(state, 2);
	}
	
	return 0;
}

MYMMOAIFBTournament::MYMMOAIFBTournament() {
	RTTI_SINGLE(MOAILuaObject)
}

MYMMOAIFBTournament::~MYMMOAIFBTournament() {
}

void MYMMOAIFBTournament::RegisterLuaClass(MOAILuaState& state) {
	state.SetField ( -1, "RETRIEVED_SCORES",	( u32 )RETRIEVED_SCORES );
	
	luaL_Reg regTable[] = {
		{ "init", 					_init},
		{ "login",					_login },
		{ "isProcessing",			_isProcessing },
		{ "setListener",			_setListener },
		{NULL, NULL}
	};

	luaL_register(state, 0, regTable);
}


void MYMMOAIFBTournament::notifyFBDidRetrieveScores(cc8* jsonScores, cc8* jsonUser, cc8* jsonPermissions) {
	MOAILuaRef& callback = this->mListeners[RETRIEVED_SCORES];
		
	if (callback) {
		MOAILuaStateHandle state = callback.GetSelf();
		
		lua_pushstring(state, jsonScores);
		lua_pushstring(state, jsonUser);
		lua_pushstring(state, jsonPermissions);
		state.DebugCall(3, 0);
	}
}

extern "C" void Java_com_meyume_moai_fbtournament_MYMFBTournament_AKUMYMNotifyFBTournamentDidRetrieveScores ( JNIEnv* env, jclass obj, jstring jsonScores, jstring jsonUser, jstring jsonPermissions ) {
	const char *resultJsonScores = env->GetStringUTFChars(jsonScores, 0);
	const char *resultJsonUser = env->GetStringUTFChars(jsonUser, 0);
	const char *resultJsonPermissions = env->GetStringUTFChars(jsonPermissions, 0);
	cc8* scores = (cc8*)resultJsonScores;
	cc8* user = (cc8*)resultJsonUser;
	cc8* permissions = (cc8*)resultJsonPermissions;
	MYMMOAIFBTournament::Get().notifyFBDidRetrieveScores(scores, user, permissions);
}
