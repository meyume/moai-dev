/*
 * Copyright (c) Xtremics Ltd All rights reserved.
 *
 * This software is the confidential and proprietary information of Xtremics
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with Xtremics.
 */
#include "pch.h"

#include <jni.h>

#include <moaiext-android/moaiext-jni.h>
#include <mym-android/MYMMOAIFlurryAndroid.h>

extern JavaVM* jvm;

int MYMMOAIFlurryAndroid::_init(lua_State* L) {
	MOAILuaState state(L);
	
	cc8* appId = lua_tostring(state, 1);
	
	JNI_GET_ENV (jvm, env);
	
	JNI_GET_JSTRING (appId, jappId);
	
	jclass flurry = env->FindClass("com/meyume/moai/MYMFlurry");
	if (flurry == NULL) {
		USLog::Print ("MYMMOAIFlurryAndroid: Unable to find java class %s", "com/meyume/moai/MYMFlurry");
	} else {
		jmethodID init = env->GetStaticMethodID (flurry, "init", "(Ljava/lang/String;)V");
		if (init == NULL) {
			USLog::Print ("MYMMOAIFlurryAndroid: Unable to find static java method %s", "init");
		} else {
			env->CallStaticVoidMethod(flurry, init, jappId);
		}
	}
	
	return 0;
}

int MYMMOAIFlurryAndroid::_logEvent(lua_State* L) {
	MOAILuaState state(L);
	
	cc8* event = lua_tostring(state, 1);
	
	JNI_GET_ENV (jvm, env);
	
	JNI_GET_JSTRING (event, jevent);
	
	jclass flurry = env->FindClass("com/meyume/moai/MYMFlurry");
	if (flurry == NULL) {
		USLog::Print ("MYMMOAIFlurryAndroid: Unable to find java class %s", "com/meyume/moai/MYMFlurry");
	} else {
		jmethodID logEvent = env->GetStaticMethodID (flurry, "logEvent", "(Ljava/lang/String;)V");
		if (logEvent == NULL) {
			USLog::Print ("MYMMOAIFlurryAndroid: Unable to find static java method %s", "logEvent");
		} else {
			env->CallStaticVoidMethod(flurry, logEvent, jevent);
		}
	}
	
	return 0;
}

int MYMMOAIFlurryAndroid::_logEventWithParams(lua_State* L) {
	MOAILuaState state(L);
	JNI_GET_ENV (jvm, env);
	
	cc8* event = lua_tostring(state, 1);
	int count = lua_tointeger(state, 2);
	
	int top = state.GetTop ();
	jobjectArray array = (jobjectArray)env->NewObjectArray(top, env->FindClass("java/lang/String"), env->NewStringUTF(""));
	
	for ( int i = 3; i <= top; ++i ) {
		if ( state.IsType ( i, LUA_TSTRING )) {
			
			cc8* p = state.GetValue < cc8* >( i, "" );
			env->SetObjectArrayElement(array, i - 3, env->NewStringUTF(p));
		}
	}
	
	JNI_GET_JSTRING (event, jevent);
	
	jclass flurry = env->FindClass("com/meyume/moai/MYMFlurry");
	if (flurry == NULL) {
		USLog::Print ("MYMMOAIFlurryAndroid: Unable to find java class %s", "com/meyume/moai/MYMFlurry");
	} else {
		jmethodID logEventWithParams = env->GetStaticMethodID (flurry, "logEventWithParams", "(Ljava/lang/String;I[Ljava/lang/String;)V");
		if (logEventWithParams == NULL) {
			USLog::Print ("MYMMOAIFlurryAndroid: Unable to find static java method %s", "logEventWithParams");
		} else {
			env->CallStaticVoidMethod(flurry, logEventWithParams, jevent, count, array);
		}
	}
	
	return 0;
}

MYMMOAIFlurryAndroid::MYMMOAIFlurryAndroid() {
	RTTI_SINGLE(MOAILuaObject)
}

MYMMOAIFlurryAndroid::~MYMMOAIFlurryAndroid() {
}

void MYMMOAIFlurryAndroid::RegisterLuaClass(MOAILuaState& state) {
	luaL_Reg regTable[] = {
		{"init", _init},
		{"logEvent", _logEvent},
		{"logEventWithParams", _logEventWithParams},
		{NULL, NULL}
	};

	luaL_register(state, 0, regTable);
}
