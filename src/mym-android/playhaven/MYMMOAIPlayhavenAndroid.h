/*
 * Copyright (c) Xtremics Ltd All rights reserved.
 *
 * This software is the confidential and proprietary information of Xtremics
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with Xtremics.
 */
 
#include <moaicore/moaicore.h>

class MYMMOAIPlayhavenAndroid : 
	public MOAIGlobalClass < MYMMOAIPlayhavenAndroid, MOAILuaObject > {

private:
	static int _init(lua_State* L);
	static int _setListener(lua_State* L);
	static int _getContent(lua_State* L);
	static int _reportIAPSolution(lua_State* L);

public:
	DECL_LUA_SINGLETON(MYMMOAIPlayhavenAndroid);
	
	enum {
		UNLOCKED_REWARD,
		SHOULD_MAKE_IAP,
		TOTAL,
	};
	
	MOAILuaRef mListeners[TOTAL];
	
	MYMMOAIPlayhavenAndroid();
	~MYMMOAIPlayhavenAndroid();
	void RegisterLuaClass(MOAILuaState& state);
	void notifyUnlockedReward(cc8* id, int qty);
	void notifyShouldMakeIAP(cc8* id);
};
