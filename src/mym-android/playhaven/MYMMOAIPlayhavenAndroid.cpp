/*
 * Copyright (c) Xtremics Ltd All rights reserved.
 *
 * This software is the confidential and proprietary information of Xtremics
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with Xtremics.
 */
#include "pch.h"

#include <jni.h>

#include <moaiext-android/moaiext-jni.h>
#include <mym-android/playhaven/MYMMOAIPlayhavenAndroid.h>

extern JavaVM* jvm;

int MYMMOAIPlayhavenAndroid::_init(lua_State* L) {
	MOAILuaState state(L);
	
	cc8* token = lua_tostring(state, 1);
	cc8* secret = lua_tostring(state, 2);
	
	JNI_GET_ENV (jvm, env);
	
	JNI_GET_JSTRING (token, jtoken);
	JNI_GET_JSTRING (secret, jsecret);
	
	jclass playhaven = env->FindClass("com/meyume/moai/MYMPlayhaven");
	if (playhaven == NULL) {
		USLog::Print ("MYMMOAIPlayhavenAndroid: Unable to find java class %s", "com/meyume/moai/MYMPlayhaven");
	} else {
		jmethodID init = env->GetStaticMethodID (playhaven, "init", "(Ljava/lang/String;Ljava/lang/String;)V");
		if (init == NULL) {
			USLog::Print ("MYMMOAIPlayhavenAndroid: Unable to find static java method %s", "init");
		} else {
			env->CallStaticVoidMethod(playhaven, init, jtoken, jsecret);
		}
	}
	
	return 0;
}

int MYMMOAIPlayhavenAndroid::_setListener(lua_State* L) {
	MOAILuaState state(L);
	
	u32 idx = state.GetValue<u32>(1, TOTAL);
	
	if (idx < TOTAL) {
		MYMMOAIPlayhavenAndroid::Get().mListeners[idx].SetStrongRef(state, 2);
	}
	
	return 0;
}

int MYMMOAIPlayhavenAndroid::_reportIAPSolution(lua_State* L) {
	MOAILuaState state(L);
	
	int status = lua_tointeger(state, 1);
	
	JNI_GET_ENV (jvm, env);
	
	jclass playhaven = env->FindClass("com/meyume/moai/MYMPlayhaven");
	if (playhaven == NULL) {
		USLog::Print ("MYMMOAIPlayhavenAndroid: Unable to find java class %s", "com/meyume/moai/MYMPlayhaven");
	} else {
		jmethodID reportIAPSolution = env->GetStaticMethodID (playhaven, "reportIAPSolution", "(I)V");
		if (reportIAPSolution == NULL) {
			USLog::Print ("MYMMOAIPlayhavenAndroid: Unable to find static java method %s", "reportIAPSolution");
		} else {
			env->CallStaticVoidMethod(playhaven, reportIAPSolution, status);
		}
	}
	
	return 0;
}

int MYMMOAIPlayhavenAndroid::_getContent(lua_State* L) {
	MOAILuaState state(L);
	
	cc8* placement = lua_tostring(state, 1);
	
	JNI_GET_ENV (jvm, env);
	
	JNI_GET_JSTRING (placement, jplacement);
	
	jclass playhaven = env->FindClass("com/meyume/moai/MYMPlayhaven");
	if (playhaven == NULL) {
		USLog::Print ("MYMMOAIPlayhavenAndroid: Unable to find java class %s", "com/meyume/moai/MYMPlayhaven");
	} else {
		jmethodID getContent = env->GetStaticMethodID (playhaven, "getContent", "(Ljava/lang/String;)V");
		if (getContent == NULL) {
			USLog::Print ("MYMMOAIPlayhavenAndroid: Unable to find static java method %s", "getContent");
		} else {
			env->CallStaticVoidMethod(playhaven, getContent, jplacement);
		}
	}
	
	return 0;
}

MYMMOAIPlayhavenAndroid::MYMMOAIPlayhavenAndroid() {
	RTTI_SINGLE(MOAILuaObject)
}

MYMMOAIPlayhavenAndroid::~MYMMOAIPlayhavenAndroid() {
}

void MYMMOAIPlayhavenAndroid::RegisterLuaClass(MOAILuaState& state) {
	state.SetField(-1,"UNLOCKED_REWARD", (u32)UNLOCKED_REWARD);
	state.SetField(-1,"SHOULD_MAKE_IAP", (u32)SHOULD_MAKE_IAP);
	
	luaL_Reg regTable[] = {
		{"init", _init},
		{"setListener", _setListener},
		{"getContent", _getContent},
		{"reportIAPSolution", _reportIAPSolution},
		{NULL, NULL}
	};

	luaL_register(state, 0, regTable);
}

void MYMMOAIPlayhavenAndroid::notifyUnlockedReward(cc8* id, int qty) {
	MOAILuaRef& callback = this->mListeners[UNLOCKED_REWARD];
		
	if (callback) {
		MOAILuaStateHandle state = callback.GetSelf();
		lua_pushstring(state, id);
		lua_pushinteger(state, qty);
		state.DebugCall(2, 0);
	}
}

void MYMMOAIPlayhavenAndroid::notifyShouldMakeIAP(cc8* id) {
	MOAILuaRef& callback = this->mListeners[SHOULD_MAKE_IAP];
		
	if (callback) {
		MOAILuaStateHandle state = callback.GetSelf();
		lua_pushstring(state, id);
		state.DebugCall(1, 0);
	}
}

extern "C" void Java_com_meyume_moai_MYMPlayhaven_AKUMYMNotifyPHUnlockedReward(JNIEnv* env, jclass obj, jstring id, int qty) {
	MYMMOAIPlayhavenAndroid::Get().notifyUnlockedReward((cc8*)env->GetStringUTFChars(id, 0), qty);
}

extern "C" void Java_com_meyume_moai_MYMPlayhaven_AKUMYMNotifyPHShouldMakeIAP(JNIEnv* env, jclass obj, jstring id) {
	MYMMOAIPlayhavenAndroid::Get().notifyShouldMakeIAP((cc8*)env->GetStringUTFChars(id, 0));
}
