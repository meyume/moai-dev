/*
 * Copyright (c) Xtremics Ltd All rights reserved.
 *
 * This software is the confidential and proprietary information of Xtremics
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with Xtremics.
 */
 
#include <moaicore/moaicore.h>

class MYMMOAIFlurryAndroid : 
	public MOAIGlobalClass < MYMMOAIFlurryAndroid, MOAILuaObject > {

private:
	static int _init(lua_State* L);
	static int _logEvent(lua_State* L);
	static int _logEventWithParams(lua_State* L);

public:
	DECL_LUA_SINGLETON(MYMMOAIFlurryAndroid);
	
	MYMMOAIFlurryAndroid();
	~MYMMOAIFlurryAndroid();
	void RegisterLuaClass(MOAILuaState& state);

};
