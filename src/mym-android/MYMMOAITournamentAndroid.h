/*
 * Copyright (c) Xtremics Ltd All rights reserved.
 *
 * This software is the confidential and proprietary information of Xtremics
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with Xtremics.
 */
 
#include <moaicore/moaicore.h>

class MYMMOAITournamentAndroid : 
	public MOAIGlobalClass < MYMMOAITournamentAndroid, MOAILuaObject > {

private:
	static int _init(lua_State* L);
	static int _login( lua_State* L );
	static int _postScore( lua_State* L );
	static int _setListener( lua_State* L );

public:
	DECL_LUA_SINGLETON(MYMMOAITournamentAndroid);
	
	enum {
		DID_RECEIVE_DATA,
		TOTAL,
	};
	
	MOAILuaRef mListeners[TOTAL];
	
	MYMMOAITournamentAndroid();
	~MYMMOAITournamentAndroid();
	void RegisterLuaClass(MOAILuaState& state);
	
	void notifyDidReceiveData(cc8* data, cc8* action);

};
