
#include <moaicore/moaicore.h>
#import <Foundation/Foundation.h>
#import <GameCircle/GameCircle.h>

@class MYMMOAIAmazonWhispersyncListener;

//================================================================//
// MYMMOAIAmazonGameCircleIOS
//================================================================//
class MYMMOAIAmazonGameCircleIOS :
	public MOAIGlobalClass < MYMMOAIAmazonGameCircleIOS, MOAILuaObject > {
private:
		MYMMOAIAmazonWhispersyncListener *whispersyncListener;
		
		static int _setListener(lua_State* L);
		static int _showLogin(lua_State* L);
		static int _showLeaderboard(lua_State* L);
		static int _showLeaderboards(lua_State* L);
		static int _showAchievements(lua_State* L);
		static int _submitScore(lua_State* L);
		static int _unlockAchievement(lua_State* L);
		static int _enableGameCenter(lua_State* L);
		static int _resolveWhispersyncConflict(lua_State* L);
		static int _synchronize(lua_State* L);
		static int _saveGameString(lua_State* L);
		static int _getGameString(lua_State* L);
		static int _saveString(lua_State* L);
		static int _getString(lua_State* L);
		static int _saveLong(lua_State* L);
		static int _getLong(lua_State* L);
		static int _saveInt(lua_State* L);
		static int _getInt(lua_State* L);
		static int _saveDouble(lua_State* L);
		static int _getDouble(lua_State* L);
public:

	DECL_LUA_SINGLETON ( MYMMOAIAmazonGameCircleIOS );
		
		enum {
			ON_WHISPERSYNC_CONFLICTS,
			TOTAL,
		};
		
		MOAILuaRef mListeners[TOTAL];
		
		bool didInit = false;
		
		NSMutableArray *conflictableDataArray;
	
	MYMMOAIAmazonGameCircleIOS();
	~MYMMOAIAmazonGameCircleIOS();
	void RegisterLuaClass( MOAILuaState& state );
		void notifyHandleWhispersyncConflicts(cc8* map, cc8* key, cc8* localValue, cc8* cloudValue);
		void onApplicationDidFinishLaunching();
		void handleOpenUrl(NSURL *url, NSString *sourceApp);
};


@interface MYMMOAIAmazonWhispersyncListener : NSObject

- (void) registerNotification;
- (void) checkPotentialGameDataConflicts;

@end

@interface ConflictableData : NSObject {
	NSString *map;
	NSString *key;
}

@property(nonatomic, retain) NSString *map;
@property(nonatomic, retain) NSString *key;

@end
