
#import <mym-ios/amazon/MYMMOAIAmazonGameCircleIOS.h>


//================================================================//
// lua
//================================================================//

//----------------------------------------------------------------//

int MYMMOAIAmazonGameCircleIOS::_setListener(lua_State* L) {
	MOAILuaState state(L);
	
	u32 idx = state.GetValue<u32>(1, TOTAL);
	
	if (idx < TOTAL) {
		MYMMOAIAmazonGameCircleIOS::Get().mListeners[idx].SetStrongRef(state, 2);
	}
	
	return 0;
}

int MYMMOAIAmazonGameCircleIOS::_showLogin(lua_State* L) {
	MOAILuaState state(L);
	
	[[AGOverlay sharedOverlay] showGameCircle:YES];
	
	return 0;
}

int MYMMOAIAmazonGameCircleIOS::_showLeaderboard(lua_State* L) {
	MOAILuaState state(L);
	
	cc8* id = lua_tostring(state, 1);
	if (id == NULL) {
		id = "";
	}
	
	[[AGOverlay sharedOverlay] showWithLeaderboardID:[NSString stringWithUTF8String:id] animated:YES];
	
	return 0;
}

int MYMMOAIAmazonGameCircleIOS::_showLeaderboards(lua_State* L) {
	MOAILuaState state(L);
	
	[[AGOverlay sharedOverlay] showWithState:AGOverlayLeaderboards animated:YES];
	
	return 0;
}

int MYMMOAIAmazonGameCircleIOS::_showAchievements(lua_State* L) {
	MOAILuaState state(L);
	
	[[AGOverlay sharedOverlay] showWithState:AGOverlayAchievements animated:YES];
	
	return 0;
}

int MYMMOAIAmazonGameCircleIOS::_submitScore(lua_State* L) {
	MOAILuaState state(L);
	
	cc8* id = lua_tostring(state, 1);
	long score = lua_tonumber(state, 2);
	if (id == NULL) {
		id = "";
	}
	
	[[AGLeaderboard leaderboardWithID:[NSString stringWithUTF8String:id]] submitWithScore:score completionHandler:^(NSDictionary *rank, NSDictionary *rankImproved, NSError *error) {
		//
	}];
	
	return 0;
}

int MYMMOAIAmazonGameCircleIOS::_unlockAchievement(lua_State* L) {
	MOAILuaState state(L);
	
	cc8* id = lua_tostring(state, 1);
	if (id == NULL) {
		id = "";
	}
	
	[[AGAchievement achievementWithID:[NSString stringWithUTF8String:id]] updateWithProgress:100.0f completionHandler:^(BOOL newlyUnlocked, NSError *error) {
		//
	}];
	
	return 0;
}

int MYMMOAIAmazonGameCircleIOS::_enableGameCenter(lua_State* L) {
	MOAILuaState state(L);
	
	if (MYMMOAIAmazonGameCircleIOS::Get().didInit) {
		// enable GameCenter
		[GameCircle enableGameCenterWithAchievementIDMappings:nil leaderboardIDMappings:nil completionHandler:^(NSError *error) {
			//
		}];
	}
	
	return 0;
}

int MYMMOAIAmazonGameCircleIOS::_synchronize(lua_State* L) {
	MOAILuaState state(L);
	
	[[AGWhispersync sharedInstance] synchronize];
	
	return 0;
}

int MYMMOAIAmazonGameCircleIOS::_resolveWhispersyncConflict(lua_State* L) {
	MOAILuaState state(L);
	
	cc8* map = lua_tostring(state, 1);
	cc8* key = lua_tostring(state, 2);
	bool useLocal = lua_toboolean(state, 3);
	if (map == NULL) {
		map = "";
	}
	if (key == NULL) {
		key = "";
	}
	
	NSString *sMap = [NSString stringWithUTF8String:map];
	NSString *sKey = [NSString stringWithUTF8String:key];
	
	for (id o in MYMMOAIAmazonGameCircleIOS::Get().conflictableDataArray) {
		ConflictableData *d = (ConflictableData*)o;
		if ([d.map isEqualToString:sMap] && [d.key isEqualToString:sKey]) {
			AGGameDataMap *gameDataMap = [[AGWhispersync sharedInstance] gameData];
			if (strcmp(map, "") != 0) {
				AGGameDataMap *dataMap = [gameDataMap mapForKey:sMap];
				AGSyncableDeveloperString *string = [dataMap developerStringForKey:sKey];
				if (useLocal) {
					[string setValue:[string value]];
					[string markAsResolved];
				} else {
					[string setValue:[string cloudValue]];
					[string markAsResolved];
				}
			} else {
				AGSyncableDeveloperString *string = [gameDataMap developerStringForKey:sKey];
				if (useLocal) {
					[string setValue:[string value]];
					[string markAsResolved];
				} else {
					[string setValue:[string cloudValue]];
					[string markAsResolved];
				}
			}
			
			break;
		}
	}
	
	return 0;
}

int MYMMOAIAmazonGameCircleIOS::_saveGameString(lua_State* L) {
	MOAILuaState state(L);
	
	cc8* map = lua_tostring(state, 1);
	cc8* key = lua_tostring(state, 2);
	cc8* val = lua_tostring(state, 3);
	if (map == NULL) {
		map = "";
	}
	if (key == NULL) {
		key = "";
	}
	if (val == NULL) {
		val = "";
	}
	
	NSString *sMap = [NSString stringWithUTF8String:map];
	NSString *sKey = [NSString stringWithUTF8String:key];
	
	if (!MYMMOAIAmazonGameCircleIOS::Get().conflictableDataArray) {
		MYMMOAIAmazonGameCircleIOS::Get().conflictableDataArray = [[NSMutableArray arrayWithCapacity:0] retain];
	}
	
	bool exist = false;
	for (id o in MYMMOAIAmazonGameCircleIOS::Get().conflictableDataArray) {
		ConflictableData *d = (ConflictableData*)o;
		if ([d.map isEqualToString:sMap] && [d.key isEqualToString:sKey]) {
			exist = true;
			break;
		}
	}
	if (!exist) {
		ConflictableData *d = [[ConflictableData alloc] init];
		d.map = sMap;
		d.key = sKey;
		[MYMMOAIAmazonGameCircleIOS::Get().conflictableDataArray addObject:d];
	}
	
	AGGameDataMap *gameDataMap = [[AGWhispersync sharedInstance] gameData];
	if (strcmp(map, "") != 0) {
		AGGameDataMap *dataMap = [gameDataMap mapForKey:sMap];
		AGSyncableDeveloperString *string = [dataMap developerStringForKey:sKey];
		[string setValue:[NSString stringWithUTF8String:val]];
	} else {
		AGSyncableDeveloperString *string = [gameDataMap developerStringForKey:sKey];
		[string setValue:[NSString stringWithUTF8String:val]];
	}
	
	return 0;
}

int MYMMOAIAmazonGameCircleIOS::_getGameString(lua_State* L) {
	MOAILuaState state(L);
	
	cc8* map = lua_tostring(state, 1);
	cc8* key = lua_tostring(state, 2);
	if (map == NULL) {
		map = "";
	}
	if (key == NULL) {
		key = "";
	}
	
	NSString *ret = nil;
	
	AGGameDataMap *gameDataMap = [[AGWhispersync sharedInstance] gameData];
	if (strcmp(map, "") != 0) {
		AGGameDataMap *dataMap = [gameDataMap mapForKey:[NSString stringWithUTF8String:map]];
		AGSyncableDeveloperString *string = [dataMap developerStringForKey:[NSString stringWithUTF8String:key]];
		ret = [string value];
	} else {
		AGSyncableDeveloperString *string = [gameDataMap developerStringForKey:[NSString stringWithUTF8String:key]];
		ret = [string value];
	}
	
	if (ret == nil) {
		lua_pushstring(state, "");
	} else {
		lua_pushstring(state, [ret UTF8String]);
	}
	
	return 1;
}

int MYMMOAIAmazonGameCircleIOS::_saveString(lua_State* L) {
	MOAILuaState state(L);
	
	cc8* map = lua_tostring(state, 1);
	cc8* key = lua_tostring(state, 2);
	cc8* val = lua_tostring(state, 3);
	if (map == NULL) {
		map = "";
	}
	if (key == NULL) {
		key = "";
	}
	if (val == NULL) {
		val = "";
	}
	
	AGGameDataMap *gameDataMap = [[AGWhispersync sharedInstance] gameData];
	if (strcmp(map, "") != 0) {
		AGGameDataMap *dataMap = [gameDataMap mapForKey:[NSString stringWithUTF8String:map]];
		AGSyncableString *string = [dataMap latestStringForKey:[NSString stringWithUTF8String:key]];
		[string setValue:[NSString stringWithUTF8String:val]];
	} else {
		AGSyncableString *string = [gameDataMap latestStringForKey:[NSString stringWithUTF8String:key]];
		[string setValue:[NSString stringWithUTF8String:val]];
	}
	
	return 0;
}

int MYMMOAIAmazonGameCircleIOS::_getString(lua_State* L) {
	MOAILuaState state(L);
	
	cc8* map = lua_tostring(state, 1);
	cc8* key = lua_tostring(state, 2);
	if (map == NULL) {
		map = "";
	}
	if (key == NULL) {
		key = "";
	}
	
	NSString *ret = nil;
	
	AGGameDataMap *gameDataMap = [[AGWhispersync sharedInstance] gameData];
	if (strcmp(map, "") != 0) {
		AGGameDataMap *dataMap = [gameDataMap mapForKey:[NSString stringWithUTF8String:map]];
		AGSyncableString *string = [dataMap latestStringForKey:[NSString stringWithUTF8String:key]];
		ret = [string value];
	} else {
		AGSyncableString *string = [gameDataMap latestStringForKey:[NSString stringWithUTF8String:key]];
		ret = [string value];
	}
	
	if (ret == nil) {
		lua_pushstring(state, "");
	} else {
		lua_pushstring(state, [ret UTF8String]);
	}
	
	return 1;
}

int MYMMOAIAmazonGameCircleIOS::_saveLong(lua_State* L) {
	MOAILuaState state(L);
	
	cc8* map = lua_tostring(state, 1);
	cc8* key = lua_tostring(state, 2);
	long val = lua_tonumber(state, 3);
	if (map == NULL) {
		map = "";
	}
	if (key == NULL) {
		key = "";
	}
	
	AGGameDataMap *gameDataMap = [[AGWhispersync sharedInstance] gameData];
	if (strcmp(map, "") != 0) {
		AGGameDataMap *dataMap = [gameDataMap mapForKey:[NSString stringWithUTF8String:map]];
		AGSyncableNumber *number = [dataMap latestNumberForKey:[NSString stringWithUTF8String:key]];
		[number setValue:[NSNumber numberWithLong:val]];
	} else {
		AGSyncableNumber *number = [gameDataMap latestNumberForKey:[NSString stringWithUTF8String:key]];
		[number setValue:[NSNumber numberWithLong:val]];
	}
	
	return 0;
}

int MYMMOAIAmazonGameCircleIOS::_getLong(lua_State* L) {
	MOAILuaState state(L);
	
	cc8* map = lua_tostring(state, 1);
	cc8* key = lua_tostring(state, 2);
	if (map == NULL) {
		map = "";
	}
	if (key == NULL) {
		key = "";
	}
	
	long ret = 0;
	
	AGGameDataMap *gameDataMap = [[AGWhispersync sharedInstance] gameData];
	if (strcmp(map, "") != 0) {
		AGGameDataMap *dataMap = [gameDataMap mapForKey:[NSString stringWithUTF8String:map]];
		AGSyncableNumber *number = [dataMap latestNumberForKey:[NSString stringWithUTF8String:key]];
		ret = [[number value] longValue];
	} else {
		AGSyncableNumber *number = [gameDataMap latestNumberForKey:[NSString stringWithUTF8String:key]];
		ret = [[number value] longValue];
	}
	
	lua_pushnumber(state, ret);
	
	return 1;
}

int MYMMOAIAmazonGameCircleIOS::_saveInt(lua_State* L) {
	MOAILuaState state(L);
	
	cc8* map = lua_tostring(state, 1);
	cc8* key = lua_tostring(state, 2);
	int val = lua_tonumber(state, 3);
	if (map == NULL) {
		map = "";
	}
	if (key == NULL) {
		key = "";
	}
	
	AGGameDataMap *gameDataMap = [[AGWhispersync sharedInstance] gameData];
	if (strcmp(map, "") != 0) {
		AGGameDataMap *dataMap = [gameDataMap mapForKey:[NSString stringWithUTF8String:map]];
		AGSyncableNumber *number = [dataMap latestNumberForKey:[NSString stringWithUTF8String:key]];
		[number setValue:[NSNumber numberWithInt:val]];
	} else {
		AGSyncableNumber *number = [gameDataMap latestNumberForKey:[NSString stringWithUTF8String:key]];
		[number setValue:[NSNumber numberWithInt:val]];
	}
	
	return 0;
}

int MYMMOAIAmazonGameCircleIOS::_getInt(lua_State* L) {
	MOAILuaState state(L);
	
	cc8* map = lua_tostring(state, 1);
	cc8* key = lua_tostring(state, 2);
	if (map == NULL) {
		map = "";
	}
	if (key == NULL) {
		key = "";
	}
	
	long ret = 0;
	
	AGGameDataMap *gameDataMap = [[AGWhispersync sharedInstance] gameData];
	if (strcmp(map, "") != 0) {
		AGGameDataMap *dataMap = [gameDataMap mapForKey:[NSString stringWithUTF8String:map]];
		AGSyncableNumber *number = [dataMap latestNumberForKey:[NSString stringWithUTF8String:key]];
		ret = [[number value] intValue];
	} else {
		AGSyncableNumber *number = [gameDataMap latestNumberForKey:[NSString stringWithUTF8String:key]];
		ret = [[number value] intValue];
	}
	
	lua_pushnumber(state, ret);
	
	return 1;
}

int MYMMOAIAmazonGameCircleIOS::_saveDouble(lua_State* L) {
	MOAILuaState state(L);
	
	cc8* map = lua_tostring(state, 1);
	cc8* key = lua_tostring(state, 2);
	double val = lua_tonumber(state, 3);
	if (map == NULL) {
		map = "";
	}
	if (key == NULL) {
		key = "";
	}
	
	AGGameDataMap *gameDataMap = [[AGWhispersync sharedInstance] gameData];
	if (strcmp(map, "") != 0) {
		AGGameDataMap *dataMap = [gameDataMap mapForKey:[NSString stringWithUTF8String:map]];
		AGSyncableNumber *number = [dataMap latestNumberForKey:[NSString stringWithUTF8String:key]];
		[number setValue:[NSNumber numberWithDouble:val]];
	} else {
		AGSyncableNumber *number = [gameDataMap latestNumberForKey:[NSString stringWithUTF8String:key]];
		[number setValue:[NSNumber numberWithDouble:val]];
	}
	
	return 0;
}

int MYMMOAIAmazonGameCircleIOS::_getDouble(lua_State* L) {
	MOAILuaState state(L);
	
	cc8* map = lua_tostring(state, 1);
	cc8* key = lua_tostring(state, 2);
	if (map == NULL) {
		map = "";
	}
	if (key == NULL) {
		key = "";
	}
	
	long ret = 0;
	
	AGGameDataMap *gameDataMap = [[AGWhispersync sharedInstance] gameData];
	if (strcmp(map, "") != 0) {
		AGGameDataMap *dataMap = [gameDataMap mapForKey:[NSString stringWithUTF8String:map]];
		AGSyncableNumber *number = [dataMap latestNumberForKey:[NSString stringWithUTF8String:key]];
		ret = [[number value] doubleValue];
	} else {
		AGSyncableNumber *number = [gameDataMap latestNumberForKey:[NSString stringWithUTF8String:key]];
		ret = [[number value] doubleValue];
	}
	
	lua_pushnumber(state, ret);
	
	return 1;
}


//================================================================//
// MYMMOAIAmazonGameCircleIOS
//================================================================//

//----------------------------------------------------------------//
MYMMOAIAmazonGameCircleIOS::MYMMOAIAmazonGameCircleIOS () {
	RTTI_SINGLE ( MOAILuaObject )
	
	whispersyncListener = [[MYMMOAIAmazonWhispersyncListener alloc] init];
}

//----------------------------------------------------------------//
MYMMOAIAmazonGameCircleIOS::~MYMMOAIAmazonGameCircleIOS () {
	[whispersyncListener release];
}

//----------------------------------------------------------------//
void MYMMOAIAmazonGameCircleIOS::RegisterLuaClass ( MOAILuaState& state ) {
	state.SetField(-1,"ON_WHISPERSYNC_CONFLICTS", (u32)ON_WHISPERSYNC_CONFLICTS);
	
	luaL_Reg regTable [] = {
		{"setListener", _setListener},
		{"showLogin", _showLogin},
		{"showLeaderboard", _showLeaderboard},
		{"showLeaderboards", _showLeaderboards},
		{"showAchievements", _showAchievements},
		{"submitScore", _submitScore},
		{"unlockAchievement", _unlockAchievement},
		{"enableGameCenter", _enableGameCenter},
		{"resolveWhispersyncConflict", _resolveWhispersyncConflict},
		{"synchronize", _synchronize},
		{"saveGameString", _saveGameString},
		{"getGameString", _getGameString},
		{"saveString", _saveString},
		{"getString", _getString},
		{"saveLong", _saveLong},
		{"getLong", _getLong},
		{"saveInt", _saveInt},
		{"getInt", _getInt},
		{"saveDouble", _saveDouble},
		{"getDouble", _getDouble},
		{NULL, NULL}
	};

	luaL_register ( state, 0, regTable );
}

void MYMMOAIAmazonGameCircleIOS::notifyHandleWhispersyncConflicts(cc8* map, cc8* key, cc8* localValue, cc8* cloudValue) {
	MOAILuaRef& callback = this->mListeners[ON_WHISPERSYNC_CONFLICTS];
	
	if (callback) {
		MOAILuaStateHandle state = callback.GetSelf();
		
		lua_pushstring(state, map);
		lua_pushstring(state, key);
		lua_pushstring(state, localValue);
		lua_pushstring(state, cloudValue);
		
		state.DebugCall(4, 0);
	}
}

void MYMMOAIAmazonGameCircleIOS::onApplicationDidFinishLaunching() {
	[GameCircle beginWithFeatures:@[AGFeatureAchievements, AGFeatureLeaderboards, AGFeatureWhispersync] completionHandler:^(NSError *error) {
		MYMMOAIAmazonGameCircleIOS::Get().didInit = true;
		/*
		// enable GameCenter
		[GameCircle enableGameCenterWithAchievementIDMappings:nil leaderboardIDMappings:nil completionHandler:^(NSError *error) {
			//
		}];
		*/
		// init whispersync
		[whispersyncListener registerNotification];
	}];
}

void MYMMOAIAmazonGameCircleIOS::handleOpenUrl(NSURL *url, NSString *sourceApp) {
	[GameCircle handleOpenURL:url sourceApplication:sourceApp];
}

@implementation MYMMOAIAmazonWhispersyncListener

- (void) registerNotification {
	NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
	[notificationCenter addObserver:self selector:@selector(checkPotentialGameDataConflicts) name:AGWhispersyncNotificationNewDataFromCloud object:nil];
	[notificationCenter addObserver:self selector:@selector(checkPotentialGameDataConflicts) name:AGWhispersyncNotificationDataUploadedToCloud object:nil];
}

- (void) checkPotentialGameDataConflicts {
	AGGameDataMap *gameDataMap = [[AGWhispersync sharedInstance] gameData];
	
	for (id o in MYMMOAIAmazonGameCircleIOS::Get().conflictableDataArray) {
		ConflictableData *d = (ConflictableData*)o;
		if (![d.map isEqualToString:@""]) {
			AGGameDataMap *dataMap = [gameDataMap mapForKey:d.map];
			AGSyncableDeveloperString *string = [dataMap developerStringForKey:d.key];
			if ([string inConflict]) {
				MYMMOAIAmazonGameCircleIOS::Get().notifyHandleWhispersyncConflicts([d.map UTF8String], [d.key UTF8String], [[string value] UTF8String], [[string cloudValue] UTF8String]);
			}
		} else {
			AGSyncableDeveloperString *string = [gameDataMap developerStringForKey:d.key];
			if ([string inConflict]) {
				MYMMOAIAmazonGameCircleIOS::Get().notifyHandleWhispersyncConflicts([d.map UTF8String], [d.key UTF8String], [[string value] UTF8String], [[string cloudValue] UTF8String]);
			}
		}
	}
}

- (void) dealloc {
	[super dealloc];
	
	NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
	[notificationCenter removeObserver:self name:AGWhispersyncNotificationNewDataFromCloud object:nil];
	[notificationCenter removeObserver:self name:AGWhispersyncNotificationDataUploadedToCloud object:nil];
}

@end


@implementation ConflictableData

@synthesize map;
@synthesize key;

@end
