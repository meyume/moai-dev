#include <moaicore/moaicore.h>
#import "Flurry.h"

//================================================================//
// MYMMOAIPlayhavenIOS
//================================================================//
class MYMMOAIFlurryIOS :
	public MOAIGlobalClass < MYMMOAIFlurryIOS, MOAILuaObject > {
private:
		static int _init( lua_State* L );
		static int _logEvent( lua_State* L );
		static int _logEventWithParams( lua_State* L );
	
public:

	DECL_LUA_SINGLETON ( MYMMOAIFlurryIOS );
	
		NSString* _appId;

	MYMMOAIFlurryIOS();
	~MYMMOAIFlurryIOS();
	void RegisterLuaClass( MOAILuaState& state );
};
