
#import <mym-ios/flurry/MYMMOAIFlurryIOS.h>

//================================================================//
// lua
//================================================================//

//----------------------------------------------------------------//
/**	@name	init
	@text	Initialize Flurry.
	
	@in		string	appId			Available in Flurry dashboard settings.
	@out 	nil
*/
int MYMMOAIFlurryIOS::_init ( lua_State* L ) {
	
	MOAILuaState state ( L );

	cc8* appId = lua_tostring ( state, 1 );
	
	MYMMOAIFlurryIOS::Get()._appId = [NSString stringWithUTF8String:appId];
	
	[Flurry startSession:MYMMOAIFlurryIOS::Get()._appId];
	
	return 0;
}

int MYMMOAIFlurryIOS::_logEvent(lua_State *L) {
	MOAILuaState state ( L );
	
	cc8* eventName = lua_tostring ( state, 1 );
	[Flurry logEvent:[NSString stringWithUTF8String:eventName]];
	
	return 0;
}

int MYMMOAIFlurryIOS::_logEventWithParams(lua_State *L) {
	MOAILuaState state ( L );
	
	cc8* eventName = lua_tostring ( state, 1 );
	int count = lua_tointeger(state, 2);
	
	NSMutableDictionary *params = [NSMutableDictionary dictionaryWithCapacity:count * 2];
	for (int i = 1; i <= count * 2; i += 2) {
		cc8* key = lua_tostring(state, i + 2);
		cc8* val = lua_tostring(state, i + 3);
		if (key == NULL) {
			key = "";
		}
		if (val == NULL) {
			val = "";
		}
		[params setObject:[NSString stringWithUTF8String:val] forKey:[NSString stringWithUTF8String:key]];
	}
	
	[Flurry logEvent:[NSString stringWithUTF8String:eventName] withParameters:params];
	
	return 0;
}

//================================================================//
// MYMMOAIFlurryIOS
//================================================================//

//----------------------------------------------------------------//
MYMMOAIFlurryIOS::MYMMOAIFlurryIOS () {
	RTTI_SINGLE ( MOAILuaObject )
}

//----------------------------------------------------------------//
MYMMOAIFlurryIOS::~MYMMOAIFlurryIOS () {
	
}

//----------------------------------------------------------------//
void MYMMOAIFlurryIOS::RegisterLuaClass ( MOAILuaState& state ) {
	luaL_Reg regTable [] = {
		{ "init",						_init },
		{ "logEvent",					_logEvent },
		{ "logEventWithParams",			_logEventWithParams },
		{ NULL, NULL }
	};

	luaL_register ( state, 0, regTable );
}
