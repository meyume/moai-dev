
#ifndef	MOAIPLAYHAVENIOS_H
#define	MOAIPLAYHAVENIOS_H

#ifndef DISABLE_PLAYHAVEN

#include <moaicore/moaicore.h>
#import "PlayHavenSDK.h"

@class MYMMOAIPlayhavenListener;

//================================================================//
// MYMMOAIPlayhavenIOS
//================================================================//
class MYMMOAIPlayhavenIOS :
	public MOAIGlobalClass < MYMMOAIPlayhavenIOS, MOAILuaObject > {
private:
		static int _init( lua_State* L );
		static int _setListener(lua_State* L);
		static int _getContent( lua_State* L );
		static int _reportIAPSolution(lua_State* L);
	
public:

	DECL_LUA_SINGLETON ( MYMMOAIPlayhavenIOS );
	
		enum {
			UNLOCKED_REWARD,
			SHOULD_MAKE_IAP,
			TOTAL,
		};
		
		MOAILuaRef mListeners[TOTAL];
		
		NSString* _token;
		NSString* _secretKey;
		
		MYMMOAIPlayhavenListener *phListener;
		PHPurchase *phPurchase;

	MYMMOAIPlayhavenIOS();
	~MYMMOAIPlayhavenIOS();
	void RegisterLuaClass( MOAILuaState& state );
	void notifyUnlockedReward(cc8* id, int qty);
	void notifyShouldMakeIAP(cc8* id);
};


@interface MYMMOAIPlayhavenListener : NSObject<PHPublisherContentRequestDelegate> {	
}
@end

#endif  //DISABLE_PLAYHAVEN

#endif  //MOAIPLAYHAVENIOS_H
