
#ifndef DISABLE_PLAYHAVEN

#import <mym-ios/playhaven/MYMMOAIPlayhavenIOS.h>

//================================================================//
// lua
//================================================================//

//----------------------------------------------------------------//
/**	@name	init
	@text	Initialize Playhaven.
	
	@in		string	appId			Available in Playhaven dashboard settings.
	@in 	string	appSignature	Available in Playhaven dashboard settings.
	@out 	nil
*/
int MYMMOAIPlayhavenIOS::_init ( lua_State* L ) {
	
	MOAILuaState state ( L );

	cc8* token = lua_tostring ( state, 1 );
	cc8* secretKey = lua_tostring ( state, 2 );
	
	MYMMOAIPlayhavenIOS::Get()._token = [[NSString stringWithUTF8String:token] retain];
	MYMMOAIPlayhavenIOS::Get()._secretKey = [[NSString stringWithUTF8String:secretKey] retain];
	
	PHPublisherOpenRequest *request = [PHPublisherOpenRequest requestForApp:MYMMOAIPlayhavenIOS::Get()._token secret:MYMMOAIPlayhavenIOS::Get()._secretKey];
	[request send];
	
	return 0;
}

int MYMMOAIPlayhavenIOS::_setListener(lua_State* L) {
	MOAILuaState state(L);
	
	u32 idx = state.GetValue<u32>(1, TOTAL);
	
	if (idx < TOTAL) {
		MYMMOAIPlayhavenIOS::Get().mListeners[idx].SetStrongRef(state, 2);
	}
	
	return 0;
}

int MYMMOAIPlayhavenIOS::_getContent(lua_State* L) {
	MOAILuaState state ( L );
	
	cc8* placement = lua_tostring(state, 1);
	
	PHPublisherContentRequest *request = [PHPublisherContentRequest requestForApp:MYMMOAIPlayhavenIOS::Get()._token secret:MYMMOAIPlayhavenIOS::Get()._secretKey placement:[NSString stringWithUTF8String:placement] delegate:MYMMOAIPlayhavenIOS::Get().phListener];
	[request send];
	
	lua_pushboolean ( state, true );
	
	return 1;
}

int MYMMOAIPlayhavenIOS::_reportIAPSolution(lua_State* L) {
	MOAILuaState state ( L );
	
	int status = lua_tointeger(state, 1);
	
	if (MYMMOAIPlayhavenIOS::Get().phPurchase) {
		PHPurchaseResolutionType resolution = PHPurchaseResolutionBuy;
		if (status > 0) {
			resolution = PHPurchaseResolutionCancel;
		} else if (status < 0) {
			resolution = PHPurchaseResolutionError;
		}
		[MYMMOAIPlayhavenIOS::Get().phPurchase reportResolution:resolution];
		
		[MYMMOAIPlayhavenIOS::Get().phPurchase release];
		MYMMOAIPlayhavenIOS::Get().phPurchase = nil;
	}
	
	return 0;
}

//================================================================//
// MYMMOAIPlayhavenIOS
//================================================================//

//----------------------------------------------------------------//
MYMMOAIPlayhavenIOS::MYMMOAIPlayhavenIOS () {
	RTTI_SINGLE ( MOAILuaObject )
	
	phListener = [[MYMMOAIPlayhavenListener alloc] init];
}

//----------------------------------------------------------------//
MYMMOAIPlayhavenIOS::~MYMMOAIPlayhavenIOS () {
	
}

//----------------------------------------------------------------//
void MYMMOAIPlayhavenIOS::RegisterLuaClass ( MOAILuaState& state ) {
	state.SetField(-1,"UNLOCKED_REWARD", (u32)UNLOCKED_REWARD);
	state.SetField(-1,"SHOULD_MAKE_IAP", (u32)SHOULD_MAKE_IAP);
	
	luaL_Reg regTable [] = {
		{"init", _init},
		{"setListener", _setListener},
		{"getContent", _getContent},
		{"reportIAPSolution", _reportIAPSolution},
		{NULL, NULL}
	};

	luaL_register ( state, 0, regTable );
}

void MYMMOAIPlayhavenIOS::notifyUnlockedReward(cc8* id, int qty) {
	MOAILuaRef& callback = this->mListeners[UNLOCKED_REWARD];
	
	if (callback) {
		MOAILuaStateHandle state = callback.GetSelf();
		lua_pushstring(state, id);
		lua_pushinteger(state, qty);
		state.DebugCall(2, 0);
	}
}

void MYMMOAIPlayhavenIOS::notifyShouldMakeIAP(cc8* id) {
	MOAILuaRef& callback = this->mListeners[SHOULD_MAKE_IAP];
	
	if (callback) {
		MOAILuaStateHandle state = callback.GetSelf();
		lua_pushstring(state, id);
		state.DebugCall(1, 0);
	}
}


@implementation MYMMOAIPlayhavenListener

/*
-(void)requestWillGetContent:(PHPublisherContentRequest *)request {
	
}

-(void)requestDidGetContent:(PHPublisherContentRequest *)request {
	
}

-(void)request:(PHPublisherContentRequest *)request contentWillDisplay:(PHContent *)content {
	
}

-(void)request:(PHPublisherContentRequest *)request contentDidDisplay:(PHContent *)content {
	
}

-(void)request:(PHPublisherContentRequest *)request contentDidDismissWithType:(PHPublisherContentDismissType *)type {
	
}

-(void)request:(PHPublisherContentRequest *)request didFailWithError:(NSError *)error {
	
}
*/
-(void)request:(PHPublisherContentRequest *)request unlockedReward:(PHReward *)reward {
	MYMMOAIPlayhavenIOS::Get().notifyUnlockedReward([reward.name UTF8String], reward.quantity);
}

-(void)request:(PHPublisherContentRequest *)request makePurchase:(PHPurchase *)purchase {
	MYMMOAIPlayhavenIOS::Get().phPurchase = purchase;
	MYMMOAIPlayhavenIOS::Get().notifyShouldMakeIAP([purchase.productIdentifier UTF8String]);
}

@end

#endif