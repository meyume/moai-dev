
#include <moaicore/moaicore.h>
#import "MYMTournamentClient.h"

@class MYMMOAITournamentListener;

//================================================================//
// MYMMOAITournamentIOS
//================================================================//
class MYMMOAITournamentIOS :
	public MOAIGlobalClass < MYMMOAITournamentIOS, MOAILuaObject > {
private:
		MYMMOAITournamentListener *mListener;
		
		static int _init( lua_State* L );
		static int _login( lua_State* L );
		static int _postScore( lua_State* L );
		static int _setListener( lua_State* L );
	
public:

	DECL_LUA_SINGLETON ( MYMMOAITournamentIOS );
		
		enum {
			CONNECTION_FAILED,
			DID_RECEIVE_REPONSE,
			DID_RECEIVE_DATA,
			TOTAL
		};
		
		MOAILuaRef		mListeners [ TOTAL ];
	
	MYMMOAITournamentIOS();
	~MYMMOAITournamentIOS();
	void RegisterLuaClass( MOAILuaState& state );
		void notifyConnectionFailed(NSError *error, NSString *action);
		void notifyDidReceiveResponse(int responseCode, NSString *action);
		void notifyDidReceiveData(NSString *data, NSString *action);
};

@interface MYMMOAITournamentListener : NSObject<MYMTournamentClientDelegate>

@end
