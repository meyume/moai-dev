//
//  MYMTournamentClient.h
//  libmoai
//
//  Created by An Nguyen on 07/08/2013.
//  Copyright (c) 2013 An Nguyen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSData+CommonCrypto.h"
#import "NSData+Base64.h"

@protocol MYMTournamentClientDelegate <NSObject>

- (void) connectionFailedWithError:(NSError*)error forAction:(NSString*)action;
- (void) didReceiveResponse:(int)responseCode forAction:(NSString*)action;
- (void) didReceiveData:(NSString*)data forAction:(NSString*)action;

@end

@interface MYMTournamentClient : NSObject<NSURLConnectionDataDelegate, NSURLConnectionDelegate> {
	id<MYMTournamentClientDelegate> delegate;
	NSString *encryptionKey;
	NSString *serverUrl;
}

@property(nonatomic, assign) id<MYMTournamentClientDelegate> delegate;
@property(nonatomic, retain) NSString *encryptionKey;
@property(nonatomic, retain) NSString *serverUrl;

+ (id) getInstance;

- (BOOL) login:(NSString*)jsonData;

- (BOOL) postScore:(NSString*)jsonData;

@end

@interface MYMURLConnection : NSURLConnection {
	NSString *action;
	NSMutableData *responseData;
}
@property(nonatomic, retain) NSString *action;
@property(nonatomic, retain) NSMutableData *responseData;

@end
