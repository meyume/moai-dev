//
//  MYMTournamentClient.m
//  libmoai
//
//  Created by An Nguyen on 07/08/2013.
//  Copyright (c) 2013 An Nguyen. All rights reserved.
//


#import "MYMTournamentClient.h"


@implementation MYMURLConnection

@synthesize action;
@synthesize responseData;

@end


@interface MYMTournamentClient()

- (BOOL) sendData:(NSString*)jsonData forAction:(NSString*)action;

@end

@implementation MYMTournamentClient

@synthesize delegate;
@synthesize encryptionKey;
@synthesize serverUrl;

static MYMTournamentClient *instance;

+ (id) getInstance {
	if (instance == nil) {
		instance = [[MYMTournamentClient alloc] init];
	}
	
	return instance;
}

// Invoke the login action.
// Return TRUE if the request sent successfully; otherwise return FALSE.
- (BOOL) login:(NSString*)jsonData {
	return [self sendData:jsonData forAction:@"login"];
}

// Post score to server.
// Return TRUE if the request sent successfully; otherwise return FALSE.
- (BOOL) postScore:(NSString*)jsonData {
	return [self sendData:jsonData forAction:@"postScore"];
}

// Send data to server.
// Return TRUE if the request sent successfully; otherwise return FALSE.
- (BOOL) sendData:(NSString*)jsonData forAction:(NSString*)action {
	NSString *jsonString = [NSString stringWithFormat:@"{\"action\":\"%@\",\"data\":%@}", action, jsonData];
	NSData *jsonBody = [NSData dataWithBytes:[jsonString cStringUsingEncoding:NSUTF8StringEncoding] length:[jsonString length]];
	
	NSError *error = nil;
	NSData *encryptedData = [jsonBody AES256EncryptedDataUsingKey:encryptionKey error:&error];
	if (error) {
		NSLog(@"---- failed to encrypt data: %@", [error localizedDescription]);
		
		return NO;
	}
	
	NSString *encryptedString = [encryptedData base64EncodedString];
	NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:serverUrl]];
	[request setHTTPMethod:@"POST"];
	[request setHTTPBody:[NSData dataWithBytes:[encryptedString cStringUsingEncoding:NSASCIIStringEncoding] length:[encryptedString length]]];
	
	MYMURLConnection *con = [[MYMURLConnection alloc] initWithRequest:request delegate:self];
	[con setAction:action];
	[con start];
	
	return YES;
}

// Callback when a connection failed to load its request successfully.
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
	NSLog(@"---- MYMTournamentClient : connection didFailWithError: %@", [error localizedDescription]);
	if (delegate) {
		MYMURLConnection *con = (MYMURLConnection*)connection;
		[delegate connectionFailedWithError:error forAction:con.action];
	}
}

// Callback when received response from server
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	if (delegate) {
		NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*)response;
		MYMURLConnection *con = (MYMURLConnection*)connection;
		[delegate didReceiveResponse:httpResponse.statusCode forAction:con.action];
	}
}

// Callback when received response data from server
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	MYMURLConnection *con = (MYMURLConnection*)connection;
	if (con.responseData == nil) {
		con.responseData = [[NSMutableData alloc] initWithData:data];
	} else {
		[con.responseData appendData:data];
	}
}

// Callback when connection did finish loading
- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
	if (delegate) {
		MYMURLConnection *con = (MYMURLConnection*)connection;
		[delegate didReceiveData:[[NSString alloc] initWithData:con.responseData encoding:NSUTF8StringEncoding] forAction:con.action];
	}
}

@end
