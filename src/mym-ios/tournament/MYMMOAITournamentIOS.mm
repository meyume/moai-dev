
#import <mym-ios/tournament/MYMMOAITournamentIOS.h>

//================================================================//
// lua
//================================================================//

//----------------------------------------------------------------//
int MYMMOAITournamentIOS::_init ( lua_State* L ) {
	
	MOAILuaState state ( L );

	cc8* serverUrl = lua_tostring ( state, 1 );
	cc8* secretKey = lua_tostring ( state, 2 );
	
	[[MYMTournamentClient getInstance] setServerUrl:[NSString stringWithUTF8String:serverUrl]];
	[[MYMTournamentClient getInstance] setEncryptionKey:[NSString stringWithUTF8String:secretKey]];
	[[MYMTournamentClient getInstance] setDelegate:MYMMOAITournamentIOS::Get().mListener];
	
	return 0;
}

int MYMMOAITournamentIOS::_login( lua_State* L ) {
	MOAILuaState state ( L );
	
	cc8* jsonData = lua_tostring(state, 1);
	
	bool ret = [[MYMTournamentClient getInstance] login:[NSString stringWithUTF8String:jsonData]];
	
	lua_pushboolean(state, ret);
	
	return 1;
}

int MYMMOAITournamentIOS::_postScore( lua_State* L ) {
	MOAILuaState state ( L );
	
	cc8* jsonData = lua_tostring(state, 1);
	
	bool ret = [[MYMTournamentClient getInstance] postScore:[NSString stringWithUTF8String:jsonData]];
	
	lua_pushboolean(state, ret);
	
	return 1;
}

// set listener for callback to LUA
int MYMMOAITournamentIOS::_setListener ( lua_State* L ) {
	
	MOAILuaState state ( L );
	
	u32 idx = state.GetValue < u32 >( 1, TOTAL );
	
	if ( idx < TOTAL ) {
		
		MYMMOAITournamentIOS::Get ().mListeners [ idx ].SetStrongRef ( state, 2 );
	}
	
	return 0;
}

//================================================================//
// MYMMOAITournamentIOS
//================================================================//

//----------------------------------------------------------------//
MYMMOAITournamentIOS::MYMMOAITournamentIOS () {
	RTTI_SINGLE ( MOAILuaObject )
	
	mListener = [[MYMMOAITournamentListener alloc] init];
}

//----------------------------------------------------------------//
MYMMOAITournamentIOS::~MYMMOAITournamentIOS () {
	[mListener release];
}

//----------------------------------------------------------------//
void MYMMOAITournamentIOS::RegisterLuaClass ( MOAILuaState& state ) {
	state.SetField ( -1, "CONNECTION_FAILED",		( u32 )CONNECTION_FAILED );
	state.SetField ( -1, "DID_RECEIVE_REPONSE",		( u32 )DID_RECEIVE_REPONSE );
	state.SetField ( -1, "DID_RECEIVE_DATA",		( u32 )DID_RECEIVE_DATA );
	
	luaL_Reg regTable [] = {
		{ "init",				_init },
		{ "login",				_login },
		{ "postScore",			_postScore },
		{ "setListener",		_setListener },
		{ NULL, NULL }
	};

	luaL_register ( state, 0, regTable );
}

void MYMMOAITournamentIOS::notifyConnectionFailed(NSError *error, NSString *action) {
	MOAILuaRef& callback = this->mListeners [ CONNECTION_FAILED ];
	if (callback) {
		MOAILuaStateHandle state = callback.GetSelf ();
		
		lua_pushstring(state, [action cStringUsingEncoding:NSUTF8StringEncoding]);
		lua_pushstring(state, [[error localizedDescription] cStringUsingEncoding:NSUTF8StringEncoding]);
		state.DebugCall ( 2, 0 );
	}
}

void MYMMOAITournamentIOS::notifyDidReceiveResponse(int responseCode, NSString *action) {
	MOAILuaRef& callback = this->mListeners [ DID_RECEIVE_REPONSE ];
	if (callback) {
		MOAILuaStateHandle state = callback.GetSelf ();
		
		lua_pushstring(state, [action cStringUsingEncoding:NSUTF8StringEncoding]);
		lua_pushinteger(state, responseCode);
		state.DebugCall ( 2, 0 );
	}
}

void MYMMOAITournamentIOS::notifyDidReceiveData(NSString *data, NSString *action) {
	MOAILuaRef& callback = this->mListeners [ DID_RECEIVE_DATA ];
	if (callback) {
		MOAILuaStateHandle state = callback.GetSelf ();
		
		lua_pushstring(state, [action cStringUsingEncoding:NSUTF8StringEncoding]);
		lua_pushstring(state, [data cStringUsingEncoding:NSUTF8StringEncoding]);
		state.DebugCall ( 2, 0 );
	}
}

@implementation MYMMOAITournamentListener

- (void) connectionFailedWithError:(NSError*)error forAction:(NSString*)action {
	MYMMOAITournamentIOS::Get().notifyConnectionFailed(error, action);
}

- (void) didReceiveResponse:(int)responseCode forAction:(NSString*)action {
	MYMMOAITournamentIOS::Get().notifyDidReceiveResponse(responseCode, action);
}

- (void) didReceiveData:(NSString*)data forAction:(NSString*)action {
	MYMMOAITournamentIOS::Get().notifyDidReceiveData(data, action);
}

@end
