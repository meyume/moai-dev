// Copyright (c) 2010-2011 Zipline Games, Inc. All Rights Reserved.
// http://getmoai.com

#include <moaiext-android/MOAIAppAndroid.h>
#include <moaiext-android/MOAIDialogAndroid.h>
#include <moaiext-android/MOAIAdColonyAndroid.h>
#include <moaiext-android/MOAIBillingAndroid.h>
//#include <moaiext-android/MOAIChartBoostAndroid.h>
#include <moaiext-android/MOAICrittercismAndroid.h>
#include <moaiext-android/MOAIFacebookAndroid.h>
#include <moaiext-android/MOAIMoviePlayerAndroid.h>
#include <moaiext-android/MOAINotificationsAndroid.h>
#include <moaiext-android/MOAITapjoyAndroid.h>

// MeYuMe
#include <mym-android/playhaven/MYMMOAIPlayhavenAndroid.h>
#include <mym-android/MYMMOAIAudioAndroid.h>
#include <mym-android/MYMMOAIGoogleBillingAndroid.h>
#include <mym-android/MYMMOAIChartboostAndroid.h>
#include <mym-android/MYMMOAIFlurryAndroid.h>
#include <mym-android/MYMMOAIGooglePlayServices.h>
#include <mym-android/MYMMOAIFacebookAndroid.h>
#include <mym-android/MYMMOAITournamentAndroid.h>
#include <mym-android/MYMMOAIFBTournament.h>
#include <mym-android/MYMMOAIGooglePlayApkExtension.h>
#include <mym-android/MYMMOAIShareAndroid.h>
#include <mym-android/MYMMOAITwitterAndroid.h>
#include <mym-android/amazon/MYMMOAIAmazonIAP.h>
#include <mym-android/amazon/MYMMOAIAmazonGameCircle.h>
#include <mym-android/amazon/MYMMOAIAmazonAnalytics.h>